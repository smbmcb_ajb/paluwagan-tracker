<?php

    session_start();
    if (!isset($_SESSION['uname'])) {
        header("location:login.php");
    }
    include("dbconnection.php");
    $uname = $_POST['username'];
    $password = $_POST['password'];
    
    
    $sql = "SELECT * FROM users WHERE usersName = ? && usersPassword = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)) {
        
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ss",$uname,$password);
    mysqli_stmt_execute($stmt);

    $resultDatas = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_num_rows($resultDatas) > 0) {
        $data = mysqli_fetch_assoc($resultDatas);

        if ($data['usersName'] === $uname AND $data['usersPassword'] === $password) {
            if ($data["memberCategory"] == "admin") 
                {
                    header("location:admin-index.php");
                    $_SESSION['uname'] = $uname;
                    $_SESSION['password'] = $password;
                    $_SESSION['memberCat'] = "admin";
                }
            else 
                {
                    header("location:index.php");
                    $_SESSION['uname'] = $uname;
                    $_SESSION['password'] = $password;
                    $_SESSION['memberCat'] = "member";
                }
            }
        else {
            header("location:login.php?error=usernotfound");
        }
    }
    else {
        header("location:login.php?error=usernotfound");
    }
    
    mysqli_stmt_close($stmt);
?>
