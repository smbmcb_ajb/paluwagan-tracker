<?php
session_start();

if (isset($_SESSION['uname'])) {
    header("location:index.php");
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log-in</title>
    <link rel="stylesheet" href="index.css">
    
</head>
<body>
    <div class="content">
        <div class="logo">
            <h1>Paluwagan</h1><span>tracker©</span>
        </div>
        
        <div class="log-in">
            <label class="error-message" id="error-message"><p><center>invalid credentials</center></p></label>
                <?php
                    if (isset($_GET['error'])) {
                        if ($_GET['error'] == "usernotfound") {
                            echo "<script>";
                            echo "document.getElementById('error-message').style.display = 'block';";  
                            echo "</script>";    
                        }
                    }
        
                ?>
            <div class="container">
                
                <form action="loginpoint.php" method="POST">
                    <div>
                        <input type="text" name="username" id="username" autocomplete="off" placeholder="Username" autofocus></input>
                    </div>
                    <div>
                        <input type="password" name="password" id="password" placeholder="Password"></input>
                    </div>
                    <div>
                        <button id="log-in">Log-in</button>
                    </div>
                    <div class="forgot-password"><center><a href="#"><p>Forgot Password?</p></a></center></div>
                        
                    <div></div>
                </form>
            </div>
        </div>
    </div>
<script src="index.js" defer></script>    
</body>
</html>

