<?php
session_start();
if (isset($_SESSION['uname'])) {
    $username = $_SESSION['uname'];
    if (isset($_SESSION['memberCat'])) {
        if ($_SESSION['memberCat']==="member") {
            header("location:index.php");
        }
    }
}
else {
    header("location:login.php");
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="index.css">
    <script src="https://kit.fontawesome.com/51460444e6.js" crossorigin="anonymous">
       
    </script>
    
    <script>
        
        
         
        <?php
            if (isset($_GET['message'])) {
                echo "confirm('New member added successfully'); window.location.href = 'admin-index.php'";
                
            }
        ?>
        
    </script>
</head>
<body>
    
        <header class="index-header">
            <div class="index-logo">
                <h1>Paluwagan</h1><span>tracker</span>
            </div>
            <div class="index-header-username-container">
                    <h3>Welcome Admin</h3>
                    <span><h2><?php echo $username; ?></h2></span>
            
            </div>
        </header>

        <div class="index-container">
            <aside class="aside-container">
                <div class="index-username-container">
                    <h3>Welcome Admin</h3>
                    <span><h2><?php echo $username; ?></h2></span>
            
                </div>
                <nav class="index-nav-aside">
                    <ul>
                        <li><div onclick="addMember()" tabindex="1" id="add-member"><h4><i class="fa fa-plus fa-3x"></i>Add Member</h4></div></li>
                        <li><div onclick="viewMemberTab()" tabindex="1" id="view-member"><h4><i class="fas fa-eye fa-3x"></i>View Members</h4></div></li>
                        <li><div onclick="programTab() "tabindex="1" id="program-tab"><h4><i class="fa fa-envelope fa-3x"></i>Programs</h4></div></li>
                        
                    </ul>
                    
                </nav>
                <div class="index-sign-out">
                    <a href="log-out.php"><h4><i class="fa fa-sign-out fa-3x"></i>Log-out</h4></a> 
                </div>
            </aside>
            
            <main class="main-container" id="main-container">
                <div id="program-container">
                    <h2>Enrolled Programs</h2>
                    <section id="index-main-section" class="index-main-section" onclick="sayHello()">
                        <div id="index-main-section-program" class="index-main-section-program"><h3>50k paluwagan</h3></div>
                    </section>
                </div>

                <div class="viewmember" id="viewmember">
                    <div class="viewmember-child1" id="viewmember-child1">
                        <?php
                            include("dbconnection.php");
                            $sql = "SELECT * from members";
                            if ($query=mysqli_query($conn,$sql)) {
                        ?>
                                    <table>
                                <tr>
                                <th>Name</th>
                                <th>BirthDay</th>
                                <th>Number</th>
                                <th>Email Address</th>
                                </tr>
                                
                        <?php
                                while ($row=mysqli_fetch_assoc($query)) {
                                    echo "<tr>";
                                    echo "<td>".$row["memberName"]."</td>";
                                    echo "<td>".$row["birthDay"]."</td>";
                                    echo "<td>".$row["mNumber"]."</td>";
                                    echo "<td>".$row["eAddress"]."</td>";
                                    echo "<td><a class='view_member' href='member.php?id=".$row['ID']."'><i class='fa-solid fa-eye fa-2x'></i></a></td>";
                                    echo "<td><a class='view_member' href='delete-member.php?id=".$row['ID']."'><i class='fa-solid fa-trash fa-2x'></i></a></td>";
                                    echo "</tr>";


                                    
                                    
                                }
                        ?>
                                </table>
                        <?php
                            }
                        ?>
                    </div>
                </div>

                <div class="addmember" id="addnewmember">
                    <div class="newmembertitle"><h2>Add New Member</h2></div>
                    <form action="addnewmember.php" method="POST">
                        <div>
                            <input type="text" required id="newmembername" name="newmembername" class="input-set" placeholder="Name required">
                        </div>
                        
                        <div>
                            <input type="date" id="newmemberbirthday" name="newmemberbirthday" class="input-set" title="birthday">
                        </div>
                        <div>
                            <input type="tel" pattern="[0-9]{11}" required id="newmembercontact" name="newmembercontact" class="input-set" placeholder="mobile number">
                        </div>
                        <div>
                            <input type="text" id="newmemberaddress" name="newmemberaddress" class="input-set" placeholder="email address">
                        </div>
                        <!--<div>
                            <input type="text" id="newmemberaccountnumber" name="newmemberaccountnumber" class="input-set" readonly>
                        </div>
                         <div>
                            
                            <select id="newmemberprogram" name="newmemberprogram" class="input-set">
                                <option value="10K">10K</option>
                                <option value="20K">20K</option>
                                <option value="50K">50K</option>
                                <option value="50K">100K</option></option>
                            </select>
                        </div>
                        <div>
                            <input type="file" id="newmemberimage" name="newmemberimage" class="input-set">
                        </div> -->
                        <div>
                            <button id="add-new-member">ADD</button>
                        </div>
                    </form>
                </div>
            </main>  
        </div>
    

        <footer class="index-footer">
            <nav class="index-nav-footer">
                <ul>
                    <li>
                        <div onclick="addMember()" tabindex=1>
                            <i class="fa fa-plus "></i><span>Add Member</span>
                        </div>
                    </li>
                    <li>
                        <div tabindex=1 onclick="viewMemberTab()">
                            <i class="fas fa-eye"></i><span>View Members</span>
                        </div>
                    </li>
                    <li>
                        <div onclick="programTab()" tabindex=1 autofocus>
                            <i class="fa fa-envelope"></i><span>Programs</span>
                        </div>
                    </li>
                </ul>
                <div class="footer-signout" onclick="logOut()" tabindex=1>
                    <i class="fa fa-sign-out"></i><span>Log-out</span>
                </div>        
            </nav>

        </footer>
    
    <script src="index.js" defer></script>
    
    <script>
        
        
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0,10);
        });
        document.getElementById('newmemberbirthday').value = new Date().toDateInputValue();

        
    </script>
    <script>
        
        if(document.getElementById("add-member")){
        document.getElementById("add-member").addEventListener('click', function(){
            window.localStorage.setItem("page","addMember");
        })}
        if(document.getElementById("program-tab")){
            document.getElementById("program-tab").addEventListener('click', function(){
            window.localStorage.setItem("page","programTab");
        })}
        if(document.getElementById("view-member")){
        document.getElementById("view-member").addEventListener('click', function(){
            window.localStorage.setItem("page","viewMemberTab");
        })}

        window.addEventListener("load", function() {
            
            var page = window.localStorage.getItem("page");
            if (page == "programTab") {
                document.getElementById("addnewmember").focus()
                document.getElementById("addnewmember").style.display = "none"
                document.getElementById("program-container").style.display = "block"
                document.getElementById("viewmember").style.display = "none"
            }else if (page == "addMember") {
                document.getElementById("addnewmember").style.display = "block"
                document.getElementById("program-container").style.display = "none"
                document.getElementById("viewmember").style.display = "none"
            } 
            else if (page == "viewMemberTab") {
                document.getElementById("addnewmember").style.display = "none"
                document.getElementById("program-container").style.display = "none"
                document.getElementById("viewmember").style.display = "block"
            }
            
        })
    </script>
</body>
</html>