<?php

    session_start();
    if (!isset($_SESSION['uname'])) {
        header("location:login.php");
    }
    include("dbconnection.php");

    
    $newmembername = $_POST['newmembername'];
    $newmemberbirthday = $_POST["newmemberbirthday"];
    $newmembercontact = $_POST["newmembercontact"];
    $newmemberaddress = $_POST["newmemberaddress"];

    
    $sql = "INSERT INTO members(memberName,birthDay,mNumber,eAddress)VALUES(?,?,?,?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)) {
        
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ssss",$newmembername,$newmemberbirthday,$newmembercontact,$newmemberaddress);
    if (!mysqli_stmt_execute($stmt)) {
        
        die("Error creating new user" . mysqli_connect_error());
    } else {
        header("location:admin-index.php?message=newmemberaddedsuccessfully");

        
    }

    
    mysqli_stmt_close($stmt);
?>