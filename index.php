<?php
session_start();
if (isset($_SESSION['uname'])) {
    $username = $_SESSION['uname'];
    if (isset($_SESSION['memberCat'])) {
        if ($_SESSION['memberCat']==="admin") {
            header("location:admin-index.php");
        }
    }
}
else {
    header("location:login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="index.css">
    <script src="https://kit.fontawesome.com/51460444e6.js" crossorigin="anonymous"></script>
</head>
<body>
    <header class="index-header">
        <div class="index-logo">
            <h1>Paluwagan</h1><span>tracker</span>
        </div>
    </header>

    <div class="index-container">
        <aside class="aside-container">
            <div class="index-username-container">
                <h3>Welcome</h3>
                <span><h2><?php echo $username; ?></h2></span>
        
            </div>
            <nav class="index-nav-aside">
                <ul>
                    <li><div><h4><i class="fa fa-dashboard fa-3x"></i>Dashboard</h4></div></li>
                    <li><div><h4><i class="fas fa-file fa-3x"></i>My Programs</h4></div></li>
                    <li><div><h4><i class="fa fa-user fa-3x"></i>My Profile</h4></div></li>
                </ul>
                   
            </nav>
            <div class="index-sign-out">
                <a href="log-out.php"><h4><i class="fa fa-sign-out fa-3x"></i>Log-out</h4></a> 
            </div>
        </aside>
        <main class="main-container" id="main-container">
            <h2>Enrolled Programs</h2>
            <section class="index-main-section" onclick="sayHello()">
                <div id="index-main-section-program" class="index-main-section-program"><h3>50k paluwagan</h3></div>
            </section>
        </main>  
    </div>

    <footer class="index-footer">
        <nav class="index-nav-footer">
            <ul>
                <li>
                    <div>
                        <i class="fa fa-dashboard "></i><span>Dashboard</span>
                    </div>
                </li>
                <li>
                    <div>
                        <i class="fas fa-file"></i><span>My Programs</span>
                    </div>
                </li>
                <li>
                    <div>
                        <i class="fa fa-user"></i><span>My Profile</span>
                    </div>
                </li>
            </ul>
            <div class="footer-signout" onclick="logOut()"><i class="fa fa-sign-out"></i><span>Log-out</span></div>        
        </nav>

    </footer>
    <script src="index.js"></script>
</body>
</html>