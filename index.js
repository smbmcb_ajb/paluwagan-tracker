
let addNewMemberBtn = document.getElementById("add-new-member")
let newMemberName = document.getElementById("newmembername")

if (addNewMemberBtn) {
addNewMemberBtn.addEventListener("click", (e)=>{
    
    let newMemberName = document.getElementById("newmembername")
    let newMemberBirthday = document.getElementById("newmemberbirthday")
    let newMemberContact = document.getElementById("newmembercontact")
    let NewMemberAddress = document.getElementById("newmemberaddress")
    
    if(newMemberName.value == "" || newMemberName.value == null) {
        e.preventDefault()
        newMemberName.style.borderColor = "red"
        newMemberName.style.boxShadow = "0px 0px 8px 0px red"
    } else {
        window.location.href = "addnewmember.php"
    }      
})
}

    if (newMemberName) {
    newMemberName.addEventListener("input", ()=>{
        
        newMemberName.style.borderColor = "revert"
        newMemberName.style.boxShadow = "revert"
    })
}

let login = document.getElementById("log-in")

if(login) {
    login.addEventListener("click", (e)=>{
        let userName = document.getElementById("username")
        let password = document.getElementById("password")
        
        let errorMessage = document.getElementById("error-message")
        
        if (userName.value == "" || userName.value == null || password.value == "" || password.value == null) {
            e.preventDefault()
            
            errorMessage.style.display = "block"
            userName.focus()
        }
        else {
            //errorMessage.style.display = "none"
            window.location.href = "loginpoint.php"
        }
    })
}
    let userName = document.getElementById("username")
    if (userName){
        userName.addEventListener("input",function(){
            let errorMessage = document.getElementById("error-message")
            errorMessage.style.display = "none"   
        })
    }
function logOut() {
    window.location.href = "log-out.php"
}


//admin index



function addMember() {
    
    document.getElementById("program-container").style.display = "none"
    document.getElementById("addnewmember").style.display = "block"
    document.getElementById("viewmember").style.display = "none"
}

function programTab() {
    
    document.getElementById("addnewmember").style.display = "none"
    document.getElementById("program-container").style.display = "block"
    document.getElementById("viewmember").style.display = "none"
}

function viewMemberTab() {
   
    document.getElementById("program-container").style.display = "none"
    document.getElementById("addnewmember").style.display = "none"
    document.getElementById("viewmember").style.display = "block"
}
